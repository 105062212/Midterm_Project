document.addEventListener('DOMContentLoaded', function () {
    if (!Notification) {
      alert('Desktop notifications not available in your browser. Try Chromium.'); 
      return;
    }
    if (Notification.permission !== "granted")
      Notification.requestPermission();
  });

  function notifyMe() {
    if (Notification.permission !== "granted")
      Notification.requestPermission();
    else {
      var notification = new Notification('阿品の聊天室', {
        icon: 'cutesaber.jpg',
        body: "快來聊天室唷~聊天室超熱鬧的!",
      });
  
      notification.onclick = function () {
        window.open("https://tiger-s-chatroom.firebaseapp.com/room.html");      
      };
  
    }
  
  }