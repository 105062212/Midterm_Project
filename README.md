# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Topic Name]
* Key functions (add/delete)
    1. chat
    2. load message history
    3. chat with new user 
    4. 聊天室只有三個key function

* Other functions (add/delete)
    1. 加好友(可以判定有誰加過、好友存不存在)
    2. 顯示線上有誰
    3. email登入驗證
    4. 私聊功能
    5. 登入畫面有下雪
    6. 好友欄位以及線上欄位使用Jquery動畫滑出
    7. 背景音樂
    8. 訊息時間


## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
作品網址：https://tiger-s-chatroom.firebaseapp.com

報告網址：https://gitlab.com/105062212/Midterm_Project/blob/master/README.md

## Security Report (Optional)
1.使用電子郵件註冊的需要信箱驗證才可登入，如此一來可以保護自己的網頁不被惡意信箱攻擊。

2.訊息輸入不直接使用innerhtml，因此不會被HTML訊息式版面攻擊造成版面毀損。

3.好友欄位經過判斷才增加，減少database垃圾資料含量。

