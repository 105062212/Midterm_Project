function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnFacebook = document.getElementById('btnfb');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function () {
    var email = txtEmail.value;
    var password = txtPassword.value;
    firebase.auth().signInWithEmailAndPassword(email, password).then(function(){
        window.location = "room.html";
    }).catch(function(error){
        var errorMessage = error.message;
            alert(error.message);
            txtEmail.value = "";
            txtPassword.value = "";
        });
    });

    btnGoogle.addEventListener('click', function () {
        var providera = new firebase.auth.GoogleAuthProvider();
        console.log('signInWithPopup');
        firebase.auth().signInWithPopup(providera).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            window.location = "room.html";
        }).catch(function (e) {
        var errorMessage = error.message;
            alert(error.message);
            txtEmail.value = "";
            txtPassword.value = "";
        });
    });

    btnFacebook.addEventListener('click', function () {
        var providerb = new firebase.auth.FacebookAuthProvider();
        console.log('signInWithPopup');
        firebase.auth().signInWithPopup(providerb).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            window.location = "room.html";
        }).catch(function (error) {
            var errorMessage = error.message;
            alert(error.message);
            txtEmail.value = "";
            txtPassword.value = "";
        });

    });
    btnSignUp.addEventListener('click', function () {
    var email = txtEmail.value;
    var password = txtPassword.value;
    firebase.auth().createUserWithEmailAndPassword(email, password).then(function() {
        alert('註冊成功:"D');
        txtEmail.value = "";
        txtPassword.value = "";
    })
    .catch(function(error) {
        var errorMessage = error.message;
        alert(error.message);
        txtEmail.value = "";
        txtPassword.value = "";
        });
    });
}

window.onload = function () {
    initApp();
};
